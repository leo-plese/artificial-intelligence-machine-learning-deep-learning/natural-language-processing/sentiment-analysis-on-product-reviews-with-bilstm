import torch
import logging

from utils.config import Config
from configs.config_reg import CFG

logger = logging.getLogger('model_factory')
logging.basicConfig(level=logging.WARNING)



class ModelFactory:
    @staticmethod
    def create_layer(name: str, in_features: int = 0, out_features: int = 0, dropout: float = 0, layers: int = 1):
        d = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if name.lower() == 'lstm':
            return torch.nn.LSTM(in_features, out_features, dropout=dropout, num_layers=layers).to(d)
        if name.lower() == 'linear':
            return torch.nn.Linear(in_features, out_features).to(d)
        if name.lower() == 'blstm':
            return torch.nn.LSTM(in_features, out_features, dropout=dropout, bidirectional=True, num_layers=layers).to(
                d)
        if name.lower() == 'relu':
            return torch.nn.ReLU()
        if name.lower() == 'sigm':
            return torch.nn.Sigmoid()
        if name.lower() == 'softmax':
            return torch.nn.Softmax(dim=1)
        logger.log(logging.WARNING, f'Not found layer with name: {name}')

    @staticmethod
    def create_criterion(name):
        if name.lower() == 'mse':
            return torch.nn.MSELoss()
        if name.lower() == 'cross':
            return torch.nn.CrossEntropyLoss()
        logger.log(logging.WARNING, f'Not found loss function with name: {name}')

    @staticmethod
    def create_optimizer(name, params=None, lr: float = 0.001, momentum: float = 0):
        if name.lower() == 'adam':
            return torch.optim.Adam(params, lr=lr)
        if name.lower() == 'sgd':
            return torch.optim.SGD(params, lr=lr, momentum=momentum)
        logger.log(logging.WARNING, f'Not found optimizer with name: {name}')


if __name__ == '__main__':
    conf = Config.from_json(CFG)
    ModelFactory.create_optimizer(conf.train.optim)
