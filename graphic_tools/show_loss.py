from matplotlib import pyplot as plt
import logging
import os

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
# 1 . za train
loss_file = './graphic_tools/loss_class4.txt'
logger = logging.getLogger('classification_model')
logging.basicConfig(level=logging.INFO)


def save_loss(avg_train, avg_eval, first=False):
    if first:
        with open(loss_file, 'w') as f:
            f.close()
    with open(loss_file, 'a') as f:
        f.write(f'{avg_train},{avg_eval}\n')
    f.close()


def show_graphs(epoch):
    train_loss = []
    eval_loss = []
    x = []
    min_loss, min_index = 100000, -1
    with open(loss_file, 'r') as f:
        for index, line in enumerate(f, 1):
            loss = line.split(',')
            train_loss.append(float(loss[0]))
            eval_loss.append(float(loss[1]))
            if min_loss > float(loss[1]):
                min_loss = float(loss[1])
                min_index = index
            x.append(index)
    # plotting
    plt.plot(x, eval_loss, color='r', label='eval loss')
    plt.plot(x, train_loss, color='g', label='train loss')
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.title(f'Loss after {epoch} epoch')
    logger.log(logging.INFO, f'Minimal eval loss = {min_loss} at epoch {min_index}')
    plt.show()


if __name__ == '__main__':
    show_graphs(26)
