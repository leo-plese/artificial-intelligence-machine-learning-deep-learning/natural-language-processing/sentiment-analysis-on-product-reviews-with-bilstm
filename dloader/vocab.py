from torch import tensor, cuda, device


class Vocab:
    def __init__(self, reviews, max_size=-1, min_freq=0, label=False):
        """
        :param reviews: list of all instances from dataset
        :param max_size: maximum size of vocabulary
        :param min_freq: minimum frequencies to put word in vocabulary
        :param label: type of vocabulary
        """

        self.max_size = max_size
        self.reviews = reviews
        self.min_freq = min_freq
        self.itos = {}
        self.stoi = {}
        self.freq = {}

        for review in reviews:
            for word in review.instance_text:
                word = word.strip()
                if word in self.freq:
                    self.freq[word] += 1
                else:
                    self.freq[word] = 1

        frequencies = {k: v for k, v in sorted(self.freq.items(), key=lambda item: item[1], reverse=True)}

        if not label:
            self.stoi = {'<PAD>': 0, '<UNK>': 1}
            self.itos = {0: '<PAD>', 1: '<UNK>'}
            for index, (key, value) in enumerate(frequencies.items(), 2):
                if self.max_size == index and value < self.min_freq:
                    break
                self.stoi[key] = index
                self.itos[index] = key

        elif label:
            self.stoi = {1.0: 0, 2.0: 1, 3.0: 2, 4.0: 3, 5.0: 4}
            self.itos = {0: 1.0, 1: 2.0, 2: 3.0, 3: 4.0, 4: 5.0}

    def encode(self, instance):
        result = []
        if not isinstance(instance, list):
            instance = [instance]
        for el in instance:
            if el in self.stoi:
                result.append(self.stoi[el])
            else:
                result.append(self.stoi["<UNK>"])
        return tensor(result).to(device("cuda" if cuda.is_available() else "cpu"))
