import os
import logging
from utils.config import Config
import csv
from dloader.instance import Instance
from dloader.vocab import Vocab
import configs.config_reg as cfg
import spacy
import tqdm

logger = logging.getLogger('data_loader')
logging.basicConfig(level=logging.INFO)


def load_processed_data(data_config) -> list:
    """
    Loading already processed data.
    Data is saved in format word_1,...,word_n label

    :param data_config: data related configurations
    :return: list of all Instances in dataset
    """
    logger.log(logging.INFO, 'Loading preprocessed data')
    data = []
    for path in data_config.path:
        with open(path, 'r') as f:
            for row in f:
                text, label = row.split(" ")
                data.append(Instance(text.split(','), float(label.strip())))
    if len(data) > 0:
        logger.log(logging.INFO, 'Preprocessed data is loaded successfully')
    return data


def process_data(data_config) -> list:
    """
    Process data from raw datasets.
    Depending on data_config it is possible to achieve a balanced dataset
    :param data_config: data related configuration
    :return: list of all instances in dataset
    """
    data = []
    example_counter = [0] * data_config.number_of_class
    num_of_examples = float('inf')
    nlp = spacy.load("en_core_web_sm")
    logger.log(logging.INFO, os.path.abspath(os.getcwd()))
    if data_config.balanced_dataset:
        num_of_examples = data_config.number_of_examples
    for path in data_config.path:
        with open(path, 'r') as f:
            for row in tqdm.tqdm(f):
                if row.startswith('review/score'):
                    score = get_score(row)
                if row.startswith('review/text'):
                    text = get_text(nlp, row)
                    if text and score != -1 and example_counter[int(score) - 1] < num_of_examples:
                        data.append(Instance(text, score))
                        example_counter[int(score) - 1] += 1
                        if sum(example_counter) >= num_of_examples * data_config.number_of_class:
                            return data
                    else:
                        score = -1.0
    return data


def get_score(row: str) -> float:
    """
    Try parse string into float number else return -1
    :param row: string in format review/score:number
    :return: score
    """
    try:
        return float(row.split(':', 1)[1].strip())
    except ValueError:
        return -1.0


def get_text(nlp, row) -> list:
    """
    Tokenized words and removed all stopwords from comment
    :param nlp: spacy model for tokenization
    :param row: string in format review/text: comment
    :return: list of tokens
    """
    data = []
    try:
        text = row.split(':', 1)[1].strip()
        doc = nlp(text)
        for token in doc:
            if not token.is_stop:
                data.append(token.lemma_.lower())
        return data
    except IndexError:
        return []


def save_data(data, data_config):
    """
    Removing punctuation and saving list of instances in text file in format word_0,...,word_n score
    :param data:
    :param data_config:
    """
    with open(data_config.save_path, 'w') as f:
        punctuation = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
        for review in data:
            texts = ''
            for text in review.instance_text:
                if text not in punctuation:
                    if texts == '':
                        texts = text
                    else:
                        texts += ',' + text
            texts += ' ' + str(review.instance_label)
            f.write(texts)
            f.write('\n')


def load_data(data_config) -> list:
    if os.path.isfile('./data/processed/processed_data.txt'):
        return load_processed_data(data_config)
    else:
        data = process_data(data_config)
        if data_config.save_data:
            save_data(data, data_config)
        return data


if __name__ == '__main__':
    config = Config.from_json(cfg.CFG)
    process_data(config.data)
