from dataclasses import dataclass


@dataclass
class Instance:
    """Class for track examples from database"""
    instance_text: list
    instance_label: float

    def __init__(self, text: list, instance_label: float):
        """
        :param text: list of all words from comment
        :param instance_label: comment review
        """
        self.instance_text = text
        self.instance_label = instance_label
