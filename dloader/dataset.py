from torch.utils.data import Dataset
from torch import nn, randn, zeros, tensor, device, cuda

from configs.config_reg import CFG
from dloader.dataloader import load_data
from dloader.vocab import Vocab
from utils.config import Config
from torch.utils.data import DataLoader, random_split
import os
import logging


logger = logging.getLogger('dataset')
logging.basicConfig(level=logging.DEBUG)


def pad_collate_fn(batch, pad_index=0):
    d = device("cuda" if cuda.is_available() else "cpu")
    texts, labels = zip(*batch)
    lengths = tensor([len(text) for text in texts])
    return nn.utils.rnn.pad_sequence(texts, padding_value=pad_index).transpose(1, 0), tensor(labels).to(d), lengths


class NLPDataset(Dataset):
    def __init__(self, cfg):
        """

        """
        self.conf = Config.from_json(cfg)
        self.reviews = load_data(self.conf.data)
        self.text_vocabulary = Vocab(self.reviews, max_size=-1, min_freq=0)
        self.label_vocabulary = Vocab(self.reviews, max_size=-1, min_freq=0, label=True)
        self.vectors = self.generateVectorMatrix(self.text_vocabulary)
        train_data, eval_data, test_data = self.split_data()
        self.train_dl = DataLoader(train_data, collate_fn=pad_collate_fn, batch_size=self.conf.train.batch_size,
                                   shuffle=True, num_workers=self.conf.train.num_worker)
        self.eval_dl = DataLoader(eval_data, collate_fn=pad_collate_fn, batch_size=self.conf.train.batch_size,
                                  shuffle=True, num_workers=self.conf.train.num_worker)
        self.test_dl = DataLoader(test_data, collate_fn=pad_collate_fn, batch_size=self.conf.train.batch_size,
                                  shuffle=True, num_workers=self.conf.train.num_worker)

    def generateVectorMatrix(self, vocabulary):
        vectorMatrix = randn((len(vocabulary.stoi), 300))
        vectorMatrix[0] = zeros((1, 300))
        if os.path.isfile(self.conf.data.glove_path):
            with open(self.conf.data.glove_path) as f:
                for line in f:
                    value = line.split()[1:]
                    value = [float(i) for i in value]
                    key = line.split()[0]
                    if key in vocabulary.stoi:
                        vectorMatrix[vocabulary.stoi[key]] = tensor(value)
        else:
            logger.log(logging.WARNING, 'Could not find gloVe file')
        d = device("cuda" if cuda.is_available() else "cpu")
        return nn.Embedding.from_pretrained(vectorMatrix.to(d), freeze=self.conf.model.freeze_embedding,
                                            padding_idx=0).to(d)

    def split_data(self):
        ratio = self.conf.model.dataset_ratio
        if sum(ratio) != 1 or len(ratio) != 3:
            logger.log(logging.WARNING, 'Check dataset_ratio')
            logger.log(logging.INFO, 'Dataset ratio is set to default')
            ratio = [0.7, 0.1, 0.2]
        train_size = int(ratio[0] * len(self))
        eval_size = int(ratio[1] * len(self))
        test_size = len(self) - (train_size + eval_size)
        return random_split(self, [train_size, eval_size, test_size])

    def __getitem__(self, item):
        """
        :param item: instance index
        :return: Encoded instance and label
        """
        return self.text_vocabulary.encode(self.reviews[item].instance_text), self.label_vocabulary.encode(
            self.reviews[item].instance_label)

    def __len__(self):
        """
        :return: dataset length
        """
        return len(self.reviews)


if __name__ == '__main__':
    dataset = NLPDataset(CFG)
    print(len(dataset))
    print(len(dataset.train_dl))
    it = iter(dataset)
    print(next(it))
