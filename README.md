# Sentiment Analysis on Product Reviews with BiLSTM

Natural Language Processing project in Sentiment Analysis of product reviews. Implemented in Python using PyTorch and Matplotlib library.

The following 2 databases were used: Cell_Phones_&_Accessories.txt and Automotive.txt. The databases are available at https://archive.org/download/amazon-reviews-1995-2013 
The word embedding technique used was GloVe 6b 300d: https://nlp.stanford.edu/projects/glove/

The databases should be place into directory data/raw and GloVe into data/glove. Processed data are place into data/processed.
A directory named saved_model should be created, where the best model is saved during training.

The projected has been submitted to JISE (Journal of Information Science and Engineering) and is currently in review phase.

Team project: 4 members with mentor supervision.

My contribution: work on code and project documentation.

Project duration: Oct 2021 - Feb 2022.
