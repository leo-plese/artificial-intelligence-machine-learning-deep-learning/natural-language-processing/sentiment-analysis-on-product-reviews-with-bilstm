"""Model config in json format"""

CFG = {
    "data": {
        'path': ['./data/processed/processed_data.txt'],
        'glove_path': './data/glove/sst_glove_6b_300d.txt',
        'balanced_dataset': True,
        'number_of_class': 5,
        'number_of_examples': 14000,
        'save_data': True,
        'save_path': '../data/processed/processed_data.txt'
    },
    "train": {
        'batch_size': 32,
        'num_of_epoch': 200,
        'optim': 'adam',
        'lr': 0.0001,
        'clip_norm': 0.2,
        'num_worker': 0,
    },
    "model": {
        'name': 'reg',
        'freeze_embedding': True,
        'dataset_ratio': [0.7, 0.1, 0.2],
        'inputs': [300, 150, 150, 75, 1],
        'network': ['blstm', 'linear', 'linear', 'linear'],
        'num_layer': 2,
        'dropout': 0.4,
        'non_linearity': 'relu',
        'activation': 'sigm',
        'loss': 'mse'
    }
}

"""Model config in json format"""

CFG2 = {
    "data": {
        'path': ['./data/processed/processed_data.txt'],
        'glove_path': './data/glove/sst_glove_6b_300d.txt',
        'balanced_dataset': True,
        'number_of_class': 5,
        'number_of_examples': 14000,
        'save_data': True,
        'save_path': '../data/processed/processed_data.txt'
    },
    "train": {
        'batch_size': 32,
        'num_of_epoch': 200,
        'optim': 'adam',
        'lr': 0.00001,
        'clip_norm': 0.2,
        'num_worker': 0,
    },
    "model": {
        'name': 'reg2',
        'freeze_embedding': True,
        'dataset_ratio': [0.7, 0.1, 0.2],
        'inputs': [300, 150, 150, 75, 1],
        'network': ['blstm', 'linear', 'linear', 'linear'],
        'num_layer': 2,
        'dropout': 0.2,
        'non_linearity': 'relu',
        'activation': 'sigm',
        'loss': 'mse'
    }
}