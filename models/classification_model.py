from abc import ABC
from torch import nn
import torch
from configs.config_class import CFG
from dloader.dataset import NLPDataset
from utils.model_factory import ModelFactory
from utils.config import Config
import logging

logger = logging.getLogger('classification_model')
logging.basicConfig(level=logging.WARN)


class ClassificationModel(nn.Module, ABC):
    def __init__(self, cfg, vectors):
        super(ClassificationModel, self).__init__()
        self.config = Config.from_json(cfg)
        self.vectors = vectors
        self.criterion = ModelFactory.create_criterion(self.config.model.loss)
        features = self.config.model.inputs
        self.layers = self.config.model.network
        if len(features) != len(self.layers) + 1:
            logger.log(logging.WARN, 'Length of network must be equal of length of features +1!')
            exit(-1)
        self.network = []
        for i in range(len(features) - 1):
            if i > 0 and self.layers[i - 1] == 'blstm':
                self.network.append(
                    ModelFactory.create_layer(name=self.layers[i], in_features=2 * features[i],
                                              out_features=features[i + 1],
                                              dropout=self.config.model.dropout,
                                              layers=self.config.model.num_layer))
            else:
                self.network.append(
                    ModelFactory.create_layer(name=self.layers[i], in_features=features[i],
                                              out_features=features[i + 1],
                                              dropout=self.config.model.dropout,
                                              layers=self.config.model.num_layer))

        self.non_linearity = ModelFactory.create_layer(name=self.config.model.non_linearity)
        self.activation = ModelFactory.create_layer(name=self.config.model.activation)
        self.init_params()
        self.network = nn.ModuleList(self.network)

    def init_params(self):
        lstm_names = ['blstm', 'lstm']
        for layer, name in zip(self.network, self.layers):
            if name in lstm_names:
                weights = [x[1] for x in layer.named_parameters()
                           if x[0].startswith("weight")]
                biases = [x[1] for x in layer.named_parameters()
                          if x[0].startswith("bias")]

                for weight, bias in zip(weights, biases):
                    torch.nn.init.xavier_normal_(weight)
                    torch.nn.init.normal_(bias, 0., 1e-6 / 3)
            elif name == 'linear':
                torch.nn.init.kaiming_normal_(layer.weight, nonlinearity=self.config.model.non_linearity)
                torch.nn.init.normal_(layer.bias, 0., 1e-6 / 3)

    def get_params(self):
        parameters = list()
        for layer in self.network:
            parameters.extend(layer.parameters())
        parameters.extend(self.vectors.parameters())
        return parameters

    def forward(self, x):
        out = self.vectors(x).transpose(1, 0)
        hidden = None
        last_layer = None
        lstm_names = ['blstm', 'lstm']
        for layer, name in zip(self.network, self.layers):
            if name.lower() in lstm_names:
                out, hidden = layer(out, hidden)
            elif last_layer in lstm_names and name not in lstm_names:
                out = layer(out[-1])
                if layer != self.network[-1]:
                    out = self.non_linearity(out)
            else:
                out = layer(out)
                if layer != self.network[-1]:
                    out = self.non_linearity(out)
            last_layer = name
        return out

    def predict(self, x):
        logit = self.forward(x)
        predictions = self.activation(logit)
        return torch.argmax(predictions, dim=-1)

    def loss(self, y, y_):
        return self.criterion(y, y_.long())


if __name__ == '__main__':
    d = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    data = NLPDataset(CFG)
    model = ClassificationModel(CFG, data.vectors).to(d)
    a = next(iter(data.train_dl))
    model.loss(model.forward(a[0]), a[1])
