import torch
import tqdm
import logging

from dloader.dataset import NLPDataset
from models.regression_model import RegressionModel
from models.classification_model import ClassificationModel
from utils.config import Config
from configs.config_class import CFG4
# from configs.config_reg import CFG3 as rCFG
from utils.model_factory import ModelFactory
from graphic_tools import show_loss

logger = logging.getLogger('train')
logging.basicConfig(level=logging.INFO)


def train(cfg, model, train_loader, optimizer):
    d = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    config = Config.from_json(cfg)
    model.train()
    avg_train_loss = 0.0
    with tqdm.tqdm(train_loader, unit="batch") as t_loader:
        for batch in t_loader:
            data = batch[0]
            labels = batch[1]
            optimizer.zero_grad()
            loss = model.loss(model.forward(data), labels.to(d))
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), config.train.clip_norm)
            optimizer.step()
            t_loader.set_postfix(loss=loss.item())
            avg_train_loss += loss.item() * data.size(0)
    return model, optimizer, avg_train_loss / len(train_loader.sampler)


def evaluation(model, valid_data):
    model.eval()
    valid_loss = 0.0
    d = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logger.log(logging.INFO, 'Model evaluation')
    for batch in valid_data:
        data = batch[0]
        labels = batch[1]
        loss = model.loss(model.forward(data), labels.to(d))
        valid_loss += loss.item() * data.size(0)
    logger.log(logging.INFO, f'Evaluation loss= {valid_loss / len(valid_data.sampler)}')
    return valid_loss / len(valid_data.sampler)


def test(model, test_data):
    logger.log(logging.INFO, 'Start testing')
    model.eval()
    correct = 0
    all_predictions = 0
    for batch in test_data:
        data = batch[0]
        label = batch[1]
        prediction = model.predict(data)
        print('------------')
        print(prediction)
        print(label)
        print('------------')
        correct += torch.eq(prediction, label).sum().item()
        all_predictions += prediction.size(dim=0)
    logger.log(logging.INFO, f'Accuracy= {correct / all_predictions}')
    return correct / all_predictions


def start_train():
    dataset = NLPDataset(CFG4)
    model = ClassificationModel(CFG4, dataset.vectors)
    # model = RegressionModel(rCFG, dataset.vectors)
    config = Config.from_json(CFG4)
    optim = ModelFactory.create_optimizer(config.train.optim, model.parameters(), config.train.lr)
    best_loss = 100000
    first = True
    for epoch in range(config.train.num_of_epoch):
        model, optim, avg_train_loss = train(CFG4, model, dataset.train_dl, optim)
        avg_eval_loss = evaluation(model, dataset.eval_dl)
        show_loss.save_loss(avg_train=avg_train_loss, avg_eval=avg_eval_loss, first=first)
        first = False
        if avg_eval_loss < best_loss:
            logger.log(logging.INFO,
                       f'Model is saved at epoch {epoch} because it has smallest validation loss!\nloss= {avg_eval_loss}')
            best_loss = avg_eval_loss
            torch.save(model.state_dict(), f'./saved_model/{config.model.name}_{epoch}.pth')
        if epoch % 10 == 0:
            # show_loss.show_graphs(epoch)
            accuracy = test(model, dataset.test_dl)
            logger.log(logging.INFO, f'Epoch: {epoch} accuracy = {accuracy}')


def test_models():
    dataset = NLPDataset(CFG4)
    # model = RegressionModel(rCFG, dataset.vectors)
    model = ClassificationModel(CFG4, dataset.vectors)
    model.load_state_dict(torch.load('./saved_model/class3_23.pth'))
    test(model, dataset.test_dl)


if __name__ == '__main__':
    start_train()
    # test_models()
